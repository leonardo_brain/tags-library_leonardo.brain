import React from 'react'
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import PropTypes from "prop-types"



const styles = (theme) => {
  return {
    filterContainer: {
      display: "flex",
      flexWrap: "wrap",
    },
    termField: {
      flexGrow: 2,
      margin: 0,
    },
    searchingBar: {
      height: "5px",
    },
  };
};

var self;

const defaultState = {
  tags: [],
  searching: false,
  seing: false,
  selectedTags: [],
};

const defaultDescription = "ESTE ES UN TAG AUTOGENERADO";

export class TagsComponent extends React.Component {

  static propTypes = {
    handleSearch: PropTypes.func 

   };

  constructor(props) {
    super(props);
    this.handleSearch = props.handleSearch.bind(this);
    this.handleSelectTag = this.handleSelectTag.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    self = this;
    this.state = defaultState;
  }
  

  handleSelectTag(e, selectedValue) {
    if (
      selectedValue &&
      !this.isTagRepeated(selectedValue.name, this.state.selectedTags)
    ) {
      this.state.selectedTags.push(selectedValue);
      this.setState({
        selectedTags: this.state.selectedTags,
      });
    }

    if (selectedValue && selectedValue.description == defaultDescription) {
      this.state.tags.pop();
    }
  }

  handleClickCloseEdit() {
    this.setState({ seing: false });
  }

  handleDelete = (value) => () => { 
    const index = this.state.selectedTags.indexOf(value);
    if (index > -1) {
      this.state.selectedTags.splice(index, 1);
      var filterSelected = this.state.selectedTags;
    }
    this.setState({ selectedTags: filterSelected });
  };

  isTagRepeated(selectedValue, taglist) {
    for (let index = 0; index < taglist.length; index++) {
      if (taglist[index].name == selectedValue) {
        return true;
      }
    }
    return false;
  }

 
  render(){
    const {text} = this.props
    const { tags } = this.state;

    console.log("tags: ",tags)
    return (        
    <div>
    <Autocomplete
      style={{
        width: "35%",
        display: "inline-flex",
        alignContent: "center",
        alignSelf: "center",
        alignItem: "center",
      }}
      id="free-solo-auto"
      freeSolo
      label="Search"
      onInputChange={this.handleSearch}
      onChange={this.handleSelectTag}
      margin="normal"
      variant="outlined"
      options={tags}
      getOptionLabel={(option) => option.name}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Search"
          margin="normal"
          variant="outlined"
        />
      )}
      renderOption={(option, { selected }) => (
        <Card style={{ width: "100%" }}>
          <CardContent>
            <Typography variant="h6">{option.name}</Typography>
            <Typography variant="body2">{option.description}</Typography>
            <Typography variant="body2">{option.synonyms}</Typography>
          </CardContent>
        </Card>
      )}
    />
    {this.state.selectedTags &&
      this.state.selectedTags.map((sel) => (
        <Chip
          label={sel.name}
          key={sel.name}
          onDelete={this.handleDelete(sel)}
          style={{
            display: "inline-flex",
            alignContent: "center",
            alignSelf: "center",
            alignItem: "center",
          }}
        />
      ))}
  </div>
    )
  }

}

